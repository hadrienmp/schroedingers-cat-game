From [Dized](https://rules.dized.com/game/lo5Bc1qAQMOKMHacEYqWxA/p4s78T0WTauDZMxiGLRrIQ/science-cat-terminology)

# Terminology

- Box = Pinked backed cards from the RESEARCH DECK
- Doctorate = Yellow backed cards marked CAT PHYSICIST DECK
- Scientist = A player
- Active = The current player
- Research Deck = Draw pile
- Experiment = A Round of Play
- My Research = My hand
- Findings = Face up cards on the table
- Hypothesis = A Bid of Alive Cats, Dead Cats, or Empty Boxes
- Prove It! = To “call” another scientist's hypothesis into question
- Observe = Revealing all research in the experiment, the end of a round of play
- Validated = Bid confirmed
- Debunked = Bid not confirmed

# Box Distribution

- 20 ALIVE cat cards
- 20 DEAD cat cards
- 8 EMPTY box cards
- 4 HEISENBERG Uncertainty Principle cards

# Setup

## First Player

The Scientist who most recently watched a documentary is named the FIRST SCIENTIST. The First Scientist has the honor of shuffling and dealing the cards for the first experiment, as well as being the Active Scientist at the start of the game.

## Doctorate Cards

Before the game starts, the First Scientist shuffles the DOCTORATE CARDS and deals one to each scientist. The Doctorate is a special power that can only be used once each game (explained later in the rules).

## Research Deck

Then the First Scientist should shuffle the RESEARCH DECK and deal a number of BOXES (cards with the pink backs) equal to the number of players to each player, and then set the RESEARCH DECK (the remaining cards) in easy reach of all the scientists.

# Gameplay

## The Experiment

### What is the experiment?

The cards in your hand each represent a box in Erwin Schrödinger’s famous experiment (or at least a version of that experiment being carried out by cats). Each card exhibits a quantum state that the cats might be in - because we won’t really know the outcome of the experiment until we OBSERVE it.

### The HEISENBERG Uncertainty Principle

The Heisenberg is an especially powerful card because it is always what you expect it to be - ALIVE, DEAD, or EMPTY. So, if the Hypothesis is about Alive cats, the Heisenberg counts as an Alive Cat. If your Hypothesis is that the cats are dead, then it counts as a Dead Cat.

IMPORTANT NOTE: A hypothesis of Heisenbergs (the quantum wildcards) is never allowed, because you can’t see the inner workings of the cosmos!

## Propose a Hypothesis

### How to Propose a Hypothesis

The Active Scientist will make a hypothesis to the group after reviewing the cards in their hand by stating the number of either Alive Cats, Dead Cats, or Empty Boxes amongst all boxes in play (the total number of cards between all players). Remember, a hypothesis of Heisenbergs (the quantum wildcards) is never allowed.

### Example of How to Propose a Hypothesis

If you have 4 Dead Cats in your hand then you can feel comfortable starting with a hypothesis (or bid) of at least 4 Dead Cats. If you also have Heisenbergs (wild cards) in your hand remember that they always count as whatever the current Hypothesis is.

### Rules of the Hypothesis

Each scientist’s Hypothesis must be (at least) incrementally higher than the last Scientist’s Hypothesis (except for the First Scientist who may start the bidding at any value). While bidding, Dead Cats have a higher bidding value than Alive Cats and Empty Boxes are worth double the bidding value of both Alive or Dead Cats. To help you out, we’ve included a handy LAB CLIPBOARD on which you can track the current Hypothesis and see the next possible hypothesis.

So, the incremental bidding order goes: 1 Alive Cat, 1 Dead Cat, 2 Alive Cats, 2 Dead Cats, 1 Empty Box, 3 Alive Cats... and so on (see the Clipboard). Remember, Empty Boxes are worth double the value of Alive or Dead Cats. Here’s the math:

N Empty Boxes > 2N Dead Cats > 2N Alive Cats

### Example of the Rules of the Hypothesis

If Stephen Pawking bids 3 Alive Cats, then the next player, Maria Goeppert-Meower, must bid 3 (or more) Dead Cats, 4 (or more) Alive Cats, or 2 (or more) Empty Boxes. The following bid would be 5 Alive or Dead Cats, or 3 Empty Boxes. Alternatively if Stephen had bid 2 Empty Boxes Maria must bid 3 Empty Boxes, 5 Alive Cats, or 5 Dead Cats.

## Continue the Experiment

### Part of the Hypothesis

As part of their Hypothesis the Active Scientist may reveal their Doctorate or Show Findings (explained later). Once complete, the next Scientist clockwise become the Active Scientist, and the Experiment continues.

Each Scientist must in turn try to prove their own hypothesis by increasing the previous scientist’s bid or calling the last scientist’s hypothesis into question (PROVE IT!) - if they believe that the declared hypothesis is highly unlikely (or just a pack of outright lies).

## Ending the Game

### Prove it!

The Active Scientist will eventually reach a point where they feel that the Current Hypothesis is invalid, or that increasing the Hypothesis would be ludicrous. When this happens, they call the last scientist’s hypothesis UNFOUNDED and say “PROVE IT!”

The Scientist whose science prevails (either because they debunked another Scientist or were confirmed to be correct) becomes the new First Scientist in the next experiment, wins the deal, and bids in the first round.

### Example of Prove It

Albert Felinestein bids 12 Alive Cats. Of the 25 boxes in the experiment, 6 Alive Cats are shown as findings. Madame Purrie becomes the Active Scientist with a hand full of Dead Cats, and tries to DEBUNK Albert’s Hypothesis, “UNFOUNDED! Prove It!”

### Observe the Experiment

When asked to Prove It!, we OBSERVE THE EXPERIMENT - all boxes in hand are revealed and placed face-up on the table. Discard all of the boxes that do not meet the Final Hypothesis, and count the remaining Findings. The Scientist that is found to have the Wrong Hypothesis is out of the game (and disgraced in the Science Cat Community for their bad science!)

### Example of Observe the Experiment

Neil deGrasse Tabby makes a hypothesis of 5 Alive Cats out of 9 boxes. The next scientist, Sally Prride, calls this into question and all boxes are placed face up. All findings that are not Alive Cats, Heisenbergs, or active Doctorate powers that count as Alive Cats, are discarded. It turns out that there are actually 7 Alive Cats and the hypothesis is deemed valid! Sally Prride, the scientist that called this research into question, is now kicked out of the lab and out of the game. She decides this a good time to get some milk.
Next

### The End of the Experiment

This is the end of this Experiment (but not necessarily the game). All the cards are collected, shuffled, and the next experiment begins (now with one less Scientist). All unused DOCTORATES remain FACE DOWN with their respective Scientist.

The number of cards dealt is equal to the number of players left in the game. So, in a 6 player game with one player eliminated, there are now 5 players and 5 cards are dealt to each player. Play continues with the remaining Scientists each having 1 fewer box in their hand.

### The Final Experiment

The game ends when the final experiment is complete and the last Scientist either successfully debunks her peer or has her research proven valid. That Scientist is the winner and receives an honorary PHD from Cat Tech UNIVERSITY in Quantum Physics!
