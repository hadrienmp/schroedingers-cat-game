import { describe, it, expect } from 'vitest';

// TODO: evolving the state with the events will have to done afterwards

type ScientistId = string & { _: 'PlayerId' };
type Box = 'Empty';
type Hypothesis = { count: number; box: Box };
type ObserveIn = { challenger: ScientistId; hypothesis: Hypothesis };
type GameEvent = { type: 'Proven wrong'; scientist: ScientistId };
type Effect = 'Shuffle the deck';
type ObserveOut = { events: GameEvent[]; effects: Effect[] };

const observe = ({ challenger }: ObserveIn): ObserveOut => ({
	events: [{ type: 'Proven wrong', scientist: challenger }],
	effects: []
});

const mariePurry = 'Marie Purry' as ScientistId;

// Two ways are possible :
// - an event based approach where the invariants would be verified by PBT
// - a type strong approach where the tests are harder to write but verified by the compiler
describe.skip("Schroedinger's cat", () => {
	describe('Observe the experiment', () => {
		// TODO: property test, observe always shuffles the deck
		// TODO: property : observe produces one and only one proven wrong event (a priori always)
		// TODO: property: produces one and only one confirmed or debunked
		it('Challenger was proven wrong', () => {
			const result = observe({ challenger: mariePurry, hypothesis: { count: 4, box: 'Empty' } });
			expect(result.events).toContain({ type: 'Proven wrong', scientist: mariePurry });
		});
		it('Challenger was proven right', () => {
			const result = observe({ challenger: mariePurry, hypothesis: { count: 4, box: 'Empty' } });
			expect(result.events).not.toContain({ type: 'Proven wrong', scientist: mariePurry });
		});
	});
});
