import { describe, it, expect } from 'vitest';
import {
	emptyGame,
	jessicat,
	lucifur,
	type Game,
	type Box,
	type Scientist,
	cleocatra,
	type PlayingGame
} from './model';
import { Failure, Ok, type Result } from '../Result';
import { addScientist } from './AddScientist';
import { startGame } from './StartGame';

// Shuffle
// Deal cards
// First scientist makes hypothesis
// Next scientists: outbids | challenges
describe('Game', () => {});
