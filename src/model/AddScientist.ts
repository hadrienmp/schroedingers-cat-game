import { type Result, Ok, Failure } from '../Result';
import type { Scientist, Game } from './model';

export type AddScientistError =
	| 'The game has started, wait until the next one starts'
	| 'A scientist with this name is already in the game, try another one'
	| 'The game automatically starts when there are 6 scientists, join another team to play !';
type AddScientist = (scientist: Scientist) => (game: Game) => Result<Game, AddScientistError>;

export const addScientist: AddScientist = (scientist) => (game) => {
	if (game.stage === 'Ready') {
		if (game.scientists.length === 6)
			return new Failure(
				'The game automatically starts when there are 6 scientists, join another team to play !'
			);
		return new Failure('The game has started, wait until the next one starts');
	}
	if (game.scientists.includes(scientist))
		return new Failure('A scientist with this name is already in the game, try another one');

	return new Ok({ ...game, scientists: [...game.scientists, scientist] });
};
