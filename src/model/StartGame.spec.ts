import { expect, describe, it } from 'vitest';
import { emptyGame, jessicat, lucifur } from './model';
import { addScientist } from './AddScientist';
import { startGame } from './StartGame';
import { Ok } from '../Result';

// TODO: start an already started game

describe('Start game', () => {
	it('changes the stage', () => {
		const started = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(lucifur))
			.andThen(startGame)
			.orFail();
		expect(started.stage).toEqual('Ready');
	});
	it('fails when there is not a least 2 players', () => {
		const result = new Ok(emptyGame()).andThen(addScientist(jessicat)).andThen(startGame);
		expect(result.orFail).toThrowError('How can you start a game if you are not at least two ?');
	});
});
