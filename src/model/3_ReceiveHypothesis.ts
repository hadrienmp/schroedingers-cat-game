import type { Game, Box, Scientist, PlayingGame } from './model';
import { Failure, Ok, type Result } from '../Result';

export type Hypothesis = { count: number; box: Box; scientist: Scientist };
export type HypothesisError = unknown;
export type ReceiveHypothesis = (
	hypothesis: Hypothesis
) => (game: Game) => Result<PlayingGame, HypothesisError>;

function isPlaying(game: Game): game is PlayingGame {
	return Object.keys(game).includes('Playing');
}

export const receiveHypothesis: ReceiveHypothesis = (hypothesis) => (game) => {
	if (hypothesis.scientist !== game.scientists[0]) return new Failure("It's not your turn");
	const outbid = isPlaying(game) ? game.outbid : [];
	return new Ok({
		scientists: game.scientists.slice(1),
		outbid: [...outbid, game.scientists[0]],
		currentHypothesis: hypothesis,
		stage: 'Playing'
	});
};
