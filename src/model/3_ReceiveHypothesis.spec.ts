import { describe, it, expect } from 'vitest';
import {
	emptyGame,
	jessicat,
	lucifur,
	type Game,
	type Box,
	type Scientist,
	cleocatra,
	type PlayingGame
} from './model';
import { Failure, Ok, type Result } from '../Result';
import { addScientist } from './AddScientist';
import { startGame } from './StartGame';
import { receiveHypothesis, type Hypothesis } from './3_ReceiveHypothesis';

describe('Propose a hypothesis', () => {
	it('sets it as the current one', () => {
		const hypothesis: Hypothesis = { count: 3, box: 'Alive', scientist: jessicat };

		const game = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(lucifur))
			.andThen(startGame)
			.andThen(receiveHypothesis(hypothesis))
			.orFail();

		expect(game.currentHypothesis).toEqual(hypothesis);
	});

	describe('rejects hypothesis scientists that are not active', () => {
		it('the first scientist is the active one', () => {
			const game = new Ok(emptyGame())
				.andThen(addScientist(jessicat))
				.andThen(addScientist(lucifur))
				.andThen(startGame)
				.andThen(receiveHypothesis({ count: 3, box: 'Alive', scientist: lucifur }));

			expect(game.orFail).toThrowError("It's not your turn");
		});

		it('second scientist is the active one', () => {
			const game = new Ok(emptyGame())
				.andThen(addScientist(jessicat))
				.andThen(addScientist(lucifur))
				.andThen(addScientist(cleocatra))
				.andThen(startGame)
				.andThen(receiveHypothesis({ count: 3, box: 'Alive', scientist: cleocatra }));

			expect(game.orFail).toThrowError("It's not your turn");
		});
	});

	it('after a hypothesis, the next scientist becomes active and can outbid the hypothesis', () => {
		const game = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(lucifur))
			.andThen(startGame)
			.andThen(receiveHypothesis({ count: 3, box: 'Alive', scientist: jessicat }))
			.andThen(receiveHypothesis({ count: 4, box: 'Alive', scientist: lucifur }))
			.orFail();

		expect(game.currentHypothesis).toEqual({ count: 4, box: 'Alive', scientist: lucifur });
	});
});
