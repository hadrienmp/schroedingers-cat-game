import type { Hypothesis } from './3_ReceiveHypothesis';

// Data Structures
export type Box = 'Alive' | 'Dead' | 'Empty' | 'Heisenberg';
export type ResearchDeck = Box[];
export type PlayingGame = {
	scientists: Scientist[];
	stage: 'Playing';
	currentHypothesis: Hypothesis;
	outbid: Scientist[];
};
export type Game =
	| { scientists: Scientist[]; stage: 'Initialising' }
	| { scientists: Scientist[]; stage: 'Ready' }
	| PlayingGame;
export type Scientist = { nickname: string };

// Functions
export const emptyGame: () => Game = () =>
	structuredClone({ scientists: [], stage: 'Initialising' });

type IOShuffleDeck = (researchDeck: ResearchDeck) => ResearchDeck;

// Test
export const clawdia: Scientist = { nickname: 'Clawdia' };
export const jessicat: Scientist = { nickname: 'Jessicat' };
export const catrick: Scientist = { nickname: 'Catrick' };
export const purramid: Scientist = { nickname: 'Purramid' };
export const lucifur: Scientist = { nickname: 'Lucifur' };
export const purrson: Scientist = { nickname: 'Purrson' };
export const cleocatra: Scientist = { nickname: 'Cleocatra' };
