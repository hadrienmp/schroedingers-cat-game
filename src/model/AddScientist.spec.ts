import { describe, it, expect } from 'vitest';
import { Ok } from '../Result';
import {
	catrick,
	clawdia,
	cleocatra,
	emptyGame,
	jessicat,
	lucifur,
	purramid,
	purrson
} from './model';
import { addScientist } from './AddScientist';
import { startGame } from './StartGame';

describe('AddScientist', () => {
	it('adds a scientist to the list', () => {
		const next = new Ok(emptyGame()).andThen(addScientist(jessicat)).orFail();
		expect(next).toEqual(expect.objectContaining({ scientists: [jessicat] }));
	});
	it('rejects scientists with an identical nickname', () => {
		const result = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(jessicat));
		expect(result.orFail).toThrowError(
			'A scientist with this name is already in the game, try another one'
		);
	});
	it('rejects new scientists when the game started', () => {
		const result = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(clawdia))
			.andThen(startGame)
			.andThen(addScientist(catrick));
		expect(result.orFail).toThrowError('The game has started, wait until the next one starts');
	});
	it('6 is the maximum number of players', () => {
		const result = new Ok(emptyGame())
			.andThen(addScientist(jessicat))
			.andThen(addScientist(clawdia))
			.andThen(addScientist(cleocatra))
			.andThen(addScientist(purrson))
			.andThen(addScientist(lucifur))
			.andThen(addScientist(purramid))
			.andThen(startGame)
			.andThen(addScientist(catrick));
		expect(result.orFail).toThrowError(
			'The game automatically starts when there are 6 scientists, join another team to play !'
		);
	});
});
