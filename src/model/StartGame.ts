import { Failure, Ok, type Result } from '../Result';
import type { Game } from './model';

export type StartGame = (game: Game) => Result<Game, StartError>;
export type StartError = 'How can you start a game if you are not at least two ?';

export const startGame: StartGame = (game) => {
	if (game.scientists.length < 2)
		return new Failure('How can you start a game if you are not at least two ?');
	return new Ok({ ...game, stage: 'Ready' });
};
