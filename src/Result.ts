export interface Result<A, Error> {
	orFail: () => A;
	map: <B>(f: (a: A) => B) => Result<B, Error>;
	andThen: <B>(f: (a: A) => Result<B, Error>) => Result<B, Error>;
	withDefault: (f: (e: Error) => A) => A;
}

export class Ok<A, Error> implements Result<A, Error> {
	private readonly value: A;
	constructor(value: A) {
		this.value = value;
	}
	map = <B>(f: (a: A) => B) => new Ok<B, Error>(f(this.value));
	andThen = <B>(f: (a: A) => Result<B, Error>) => f(this.value);
	withDefault = () => this.value;
	orFail = () => this.value;
}

export class Failure<A, T> implements Result<A, T> {
	private readonly error: T;
	constructor(error: T) {
		this.error = error;
	}
	map = <B>() => new Failure<B, T>(this.error);
	andThen = <B>() => new Failure<B, T>(this.error);
	withDefault = (f: (e: T) => A) => f(this.error);
	orFail = () => {
		throw Error(JSON.stringify(this.error));
	};
}
